<?php
/**
 *	The template for displaying EQUIPE page content.
 */
#die('content-page-equipe.php');
$jumbotron_single_image  = get_theme_mod( 'illdy_jumbotron_enable_featured_image', true );

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'blog-post' ); ?>>
	<h1 class="blog-post-title"><?php the_title(); ?></h1>
	<?php if ( has_post_thumbnail() && $jumbotron_single_image != true ) { ?>
		<div class="blog-post-image">
			<?php the_post_thumbnail( 'illdy-blog-list' ); ?>
		</div><!--/.blog-post-image-->
	<?php } ?>
	<div class="blog-post-entry markup-format">
		<?php
		#the_content();
        if( have_rows('membro') ):
            while ( have_rows('membro') ) : the_row();
            #echo '<pre>';
        ?>
                <div class="col-sm-4 col-xs-6 cmp-membro">
                    <div class="membro-header">
                        <?php
                        $img_id = get_sub_field('membro_foto')['ID'];
                        /*var_dump(wp_get_attachment_image($img_id, 'illdy-front-page-projects'));
                        var_dump(get_sub_field('membro_foto'));
                        die('#');*/
                        ?>
                        <img src="<?=wp_get_attachment_image_src($img_id, 'illdy-front-page-projects')[0]; ?>" alt="" class="img-responsive">
                    </div>
                    <div class="membro-body">
                        <h5><?=get_sub_field('membro_nome'); ?></h5>
                        <div class="entry-content">
                            <?=get_sub_field('membro_descricao'); ?>
                        </div>
                    </div>
                </div>
        <?php
            endwhile;
        endif;
		?>
	</div><!--/.blog-post-entry.markup-format-->
</article><!--/#post-<?php the_ID(); ?>.blog-post-->
